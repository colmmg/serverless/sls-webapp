import boto3
import datetime
import json
import os
import secrets
import sys
from botocore.exceptions import ClientError

def post(event, context):
  getParameters(event)
  isAuthorized = verifyLogin()
  if isAuthorized:
    setSessionId()
    response = {
      "statusCode": 200,
      "body": json.dumps(SESSION_ID),
      "headers": {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    }
  else:
    response = {
      "statusCode": 401,
      "body": json.dumps('FAILED'),
      "headers": {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    }
  return response

def getParameters(event):
  global AWS_REGION, USERNAME, PASSWORD
  AWS_REGION = os.environ['AWS_REGION']
  USERNAME = event['queryStringParameters']['username']
  PASSWORD = event['queryStringParameters']['password']

def verifyLogin():
  secretResponse = getSecret()
  if secretResponse != None:
    secretResponseDict = json.loads(secretResponse)
  else:
    return False
  if 'Password' in secretResponseDict:
    secretPassword = secretResponseDict['Password']
  else:
    return False
  return secretPassword == PASSWORD

def getSecret():
  try:
    ec2 = boto3.client('ec2', AWS_REGION)
    client = boto3.session.Session().client(service_name='secretsmanager', region_name=AWS_REGION)
    secretid = "my-serverless-website/" + USERNAME
    resp = client.get_secret_value(SecretId=secretid)
    if 'SecretString' in resp:
      return resp['SecretString']
    else:
      return base64.b64decode(resp['SecretBinary'])
  except ClientError as err:
    return None

def setSessionId():
  global SESSION_ID
  SESSION_ID = secrets.token_urlsafe()
  try:
    dynamodb = boto3.client('dynamodb', AWS_REGION)
    epoch = epoch = datetime.datetime.utcfromtimestamp(0)
    now = datetime.datetime.utcnow()
    diff = (now - epoch).total_seconds()
    now_seconds = int(diff)
    ttl = now_seconds + 60
    dynamodb.put_item(TableName='my-serverless-website-sessions', Item={'userName':{'S':USERNAME},'session-id':{'S':SESSION_ID},'creationTime':{'N':str(now_seconds)},'ttl':{'N':str(ttl)}})
  except ClientError as err:
    return None
