import boto3
import json
import os
import sys
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr

def get(event, context):
  getParameters(event)
  getUserName()
  if USERNAME != None:
    getLogs()
    response = {
      "statusCode": 200,
      "body": json.dumps(LOGS),
      "headers": {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    }
  else:
    response = {
      "statusCode": 401,
      "body": json.dumps('FAILED'),
      "headers": {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    }
  return response

def getParameters(event):
  global AWS_REGION, SESSION_ID
  AWS_REGION = os.environ['AWS_REGION']
  SESSION_ID = event['queryStringParameters']['session-id']

def getUserName():
  global USERNAME
  try:
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('my-serverless-website-sessions')
    response = table.get_item(
      Key={
        'session-id': SESSION_ID
      }
    )
    USERNAME = response["Item"]["userName"]
  except ClientError as err:
    return None

def getLogs():
  global LOGS
  try:
    dynamodb = boto3.resource('dynamodb', AWS_REGION)
    table = dynamodb.Table('my-serverless-website-logs')
    response = table.query(
      KeyConditionExpression=Key('userName').eq(USERNAME)
    )
    LOGS = response[u'Items']
  except ClientError as err:
    return None
