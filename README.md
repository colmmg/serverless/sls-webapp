# sls-webapp
A sample serverless web application running on AWS. This [serverless](https://serverless.com/cli/) project deploys the backend components used by the project at [gitlab.com/colmmg/terraform/sls-webapp](https://gitlab.com/colmmg/terraform/sls-webapp).

# Serverless  Deploy Instructions
To deploy run:
```
sls deploy --accountid YOUR_AWS_ACCOUNT_ID
```
